<?php


namespace calderaultra\jsonld\Contexts;


/**
 * Class EDD
 * @package calderaultra\jsonld
 */
class EDD  implements  Implementation{

	/** @var \EDD_Download  */
	protected $download;

	/** @var array  */
	protected $offers = [];

	/**
	 * EDD constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param \EDD_Download $download
	 */
	public function __construct( \EDD_Download $download )
	{
		$this->download = $download;
	}

	/** @inheritdoc */
	public function create()
	{


		return CreateContext::render( 'product', $this->generateProperties(), 'edd_product' );
	}
	/** @inheritdoc */

	public function generateProperties() :array
	{
		$properties = [
			'name' => $this->download->post_title,
			'description' => $this->download->post_excerpt,
			'brand' => get_bloginfo( 'name'),
			'image' => wp_get_attachment_image_url( get_post_thumbnail_id( $this->download->get_ID())),
			'sku' => $this->download->get_sku(),
			'url' => get_permalink( $this->download->get_ID() ),
			'review' => null,
			'aggregateRating' => null,
			'offers' => $this->getOffers(),
			'gtin8' => null,
			'gtin13' => null,
			'gtin14' => null,
			'mpn' => null,
			'category' => $this->getCategories(),
			'model' => null
		];

		if( ! empty( $properties[ 'offers' ] ) ){
			foreach ( $properties[ 'offers' ] as $offer ){
				$this->offers[] = $offer->getProperties();
			}
			$filter = CreateContext::filterName( 'product', 'edd_product' );
			add_filter( $filter, function( $args ){
				$args[ 'offers' ] = $this->offers;
				return $args;
			});
		}

		$properties[ '@type' ] = 'Product';
		return $properties;
	}

	/**
	 * Create categories list
	 *
	 * @return string
	 */
	protected function getCategories()
	{

		$cats = wp_get_post_terms( $this->download->get_ID(), 'download_category' );
		if( is_array( $cats ) && ! empty( $cats ) ){
			$cats = wp_list_pluck( $cats, 'name' );
		}else{
			$cats = [];
		}

		$tags = get_terms( $this->download->get_ID(), 'download_tag' );
		if( is_array( $tags ) && ! empty( $tags ) ){
			$tags = wp_list_pluck( $tags, 'name' );
		}else{
			$tags = [];
		}

		$list = array_merge( $cats, $tags );


		return implode( ',',  $list );
	}

	/**
	 * Create offers object
	 *
	 * @since 0.0.1
	 *
	 * @return array|null
	 */
	protected function getOffers( )
	{
		if( $this->download->can_purchase() ) {
			$output = [];
			if( $this->download->is_free() ){
				$output[] = $this->createFreeOffer();
			}elseif( $this->download->has_variable_prices() ){
				foreach ( $this->download->get_prices() as $price ){
					$output[] = $this->createOffer( $price );
				}
			}else{
				$output[] = $this->createOffer( $this->download->get_price() );
			}

			return $output;
		}else{
			return null;
		}


	}

	/**
	 * Create an offer object
	 *
	 * @since 0.0.1
	 *
	 * @param $price
	 *
	 * @return \JsonLd\Context
	 */
	protected function createOffer( $price )
	{

		if( ! is_array( $price ) ){
			return $this->createFreeOffer();
		}

		$args = [
			'price' => $price[ 'amount' ],
			'priceCurrency' => 'USD',
			'priceValidUntil' => null,
			'itemOffered' => $this->download->post_title,
			'url' => get_permalink( $this->download->get_ID() ),
			'availability' => 'http://schema.org/InStock'
		];

		if( ! empty( $price[ 'name'] ) ) {
			$args ['itemOffered' ] .=  ': ' . $price[ 'name' ];
		}

		return CreateContext::create( 'offer', $args, 'edd_price' );

	}

	protected function createFreeOffer(){
		$args = [
			'price' => '0.00',
			'priceCurrency' => 'USD',
			'priceValidUntil' => null,
			'itemOffered' => $this->download->post_title,
			'url' => get_permalink( $this->download->get_ID() ),
			'availability' => 'http://schema.org/InStock'
		];

		return CreateContext::create( 'offer', $args, 'edd_price' );
	}
 }