<?php


namespace calderaultra\jsonld\Contexts;
use JsonLd\Context;


/**
 * Class CreateContext
 * @package calderaultra\jsonld\Contexts
 */
class CreateContext {

	/**
	 * Render a script tag
	 *
	 * @since 0.0.1
	 *
	 * @param string $type Context type - Must be valid JSON-LD type
	 * @param array $properties Properties to use to generate context
	 * @param string $specificType Used to create filters, be consistent.
	 *
	 * @return string
	 */
	public static function render( string $type, array $properties, string $specificType ) : string
	{
		$args = self::prepareProperties( $type, $properties, $specificType );

		return self::tag( $args );
	}

	/**
	 * Render a new context object
	 *
	 * @since 0.0.1
	 *
	 * @param string $type Context type - Must be valid JSON-LD type
	 * @param array $properties Arguments to use to generate context
	 * @param string $specificType Used to create filters, be consistent.
	 *
	 * @return Context
	 */
	public static function create( string $type, array $properties, string $specificType ) : Context
	{
		$properties = self::prepareProperties( $type, $properties, $specificType );
		return Context::create( $type, $properties );
	}

	/**
	 * Render script tag
	 *
	 * @since 0.0.1
	 *
	 * @param array $properties Properties, prepated for output
	 *
	 * @return string
	 */
	protected static function tag( array  $properties ) : string
	{
		return "<script type=\"application/ld+json\">" . json_encode($properties) . "</script>";
	}

	/**
	 * Create filter name
	 *
	 * @since 0.0.1
	 *
	 * @param string $type Context type - Must be valid JSON-LD type
	 * @param string $specificType Used to create filters, be consistent.
	 *
	 * @return string
	 */
	public static function filterName( $type, $specificType ) : string
	{
		return 'calderaultra_ldjson_context_args_' . $type . '_' . $specificType;
	}

	/**
	 * Prepare properties
	 *
	 * Applies filters and removes emapties
	 *
	 * @since 0.0.1
	 *
	 * @param string $type Context type - Must be valid JSON-LD type
	 * @param array $properties Properties to use to generate context
	 * @param string $specificType Used to create filters, be consistent.
	 *
	 * @return array
	 */
	protected static function prepareProperties( string $type, array $properties, string $specificType ) : array
	{
		/**
		 * Dynamically named filter for JSON LD properties to filter by context
		 *
		 * @since 0.0.1
		 *
		 * @param array $properties Properties to use to generate context
		 */
		$properties = apply_filters( self::filterName( $type, $specificType ), $properties );

		/**
		 * Filter for JSON LD properties to filter all contexts
		 *
		 * @since 0.0.1
		 *
		 * @param array $properties Properties to use to generate context
		 */
		$properties = apply_filters( 'calderaultra_ldjson_context_args', $properties );

		$properties = array_filter( $properties );

		return $properties;
	}
}