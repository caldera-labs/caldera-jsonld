<?php


namespace calderaultra\jsonld\Contexts;
use JsonLd\Context;


/**
 * Interface Implementation
 * @package calderaultra\jsonld\Contexts
 */
interface Implementation {

	/**
	 * Create markup
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	public function create();

	/**
	 * Generate properties to represent object as context
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function generateProperties() : array;
}