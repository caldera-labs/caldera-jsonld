<?php


namespace calderaultra\jsonld;


/**
 * Class Yoast
 * @package calderaultra\jsonld
 */
class Yoast {

	/** @var int  */
	protected $postId;

	/**
	 * Yoast constructor.
	 * @since 0.0.1
	 * 
	 * @param int $postId
	 */
	public function __construct( int $postId )
	{
		$this->postId = $postId;
	}

	/**
	 * Use Yoast's SEO meta description as description, if set
	 *
	 * @uses "calderaultra_ldjson_context_args" filter
	 *
	 * @since 0.0.1
	 *
	 * @param array $props Schema properties
	 *
	 * @return array
	 */
	public function useMetaDescription( $props )
	{
		$description = \WPSEO_Meta::get_value( 'metadesc', $this->postId );
		if (  ! empty( $description ) ) {
			$props[ 'description' ] = $description;
		}
		return $props;
	}

	/**
	 * Add Yoast's focus keyword to keywords if set
	 *
	 * @uses "calderaultra_ldjson_context_args" filter
	 *
	 * @since 0.0.1
	 *
	 * @param array $props Schema properties
	 *
	 * @return array
	 */
	public function prependFocusKeyword( $props )
	{
		$focus = \WPSEO_Meta::get_value( 'focuskw', $this->postId );

		if ( ! empty( $focus ) ) {
			if( !  isset( $props[ 'keywords' ] ) ){
				$props[ 'keywords' ] = $focus;
			}else{
				$props[ 'keywords' ] = $focus . ' ' . $props[ 'keywords' ];

			}

		}
		return $props;
	}

	/**
	 * Use Yoast's SEO title if set
	 *
	 * @uses "calderaultra_ldjson_context_args" filter
	 *
	 * @since 0.0.1
	 *
	 * @param array $props Schema properties
	 *
	 * @return array
	 */
	public function changeTitle( $props )
	{
		$title = \WPSEO_Meta::get_value( 'title', $this->postId );
		if (  ! empty( $title ) ) {
			$props[ 'name' ] = $title;
		}
		return $props;
	}
}