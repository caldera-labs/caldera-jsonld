LD+JSON Structure Markup Generator For WordPress

**Requirements**
* PHP7 is required.
* You must run `composer update` before activting.

This plugin makes use of [https://github.com/Torann/json-ld](https://github.com/Torann/json-ld) see its docs for available context types.

# Usage
For posts, pages and Easy Digital Downloads products, should just work.

## Custom Post Types
To add custom post type support, add the post type name to the array returned by the `calderaultra_ldjson_supported_post_types` filter. Note that this will not use special contexts like product or event, so you probably do not want to add an event or product post type here.

Example:
```
    add_filter( 'calderaultra_ldjson_supported_post_types', function( $postTypes ){
        $postTypes[] = 'industry';
        return $postTypes;
    });
```

### Custom Implementations
The `calderaultra_ldjson_creator` filter can be used as an early entry point to create your own structured markup generator. When using this filter, returning an object implementing `\calderaultra\jsonld\Contexts\Implementation` interface will use that object to generate markup at `wp_head`. Returning `null` will cause the default default logic to be used. Return a different truthy value to prevent anything from being generated.



Example:

```
    use \calderaultra\jsonld\Contexts\CreateContext;
    use \calderaultra\jsonld\Helpers;
    use \JsonLd\Context;
    
    add_action( 'plugins_loaded', function () {
        /**
         * Handle front page special
         */
        add_filter( 'calderaultra_ldjson_creator', function ( $creator ) {
            if ( is_front_page() ) {
    
    
                $creator = ( new class implements \calderaultra\jsonld\Contexts\Implementation {
                    public function create()
                    {
                        return CreateContext::render( 'product', $this->generateProperties(), 'caldera-forms' );
                    }
    
                    public function generateProperties() : array
                    {
                        $properties            = [
                            'name'        => 'Caldera Forms',
                            'description' => 'Drag and drop responsive form builder for contact forms and more!',
                            'image'       => Helpers::image( 36262 ),
                            'url'         => 'https://Caldera Forms.com',
                            'keywords'    => 'Caldera Forms WordPress Contact Form Drag and Drop'
    
                        ];
                        $properties[ '@type' ] = 'Product';
    
    
                        $filter = CreateContext::filterName( 'product', 'caldera-forms' );
                        add_filter( $filter, function ( $args ) {
                            $offers = [
                                CreateContext::create( 'offer', [
                                    'price'           => '0.00',
                                    'itemOffered'     => 'Caldera Forms Blogger Bundle',
                                    'priceCurrency'   => 'USD',
                                    'priceValidUntil' => null,
                                    'url'             => 'https://calderaforms.com/caldera-forms-bundles/',
                                    'availability'    => 'http://schema.org/InStock'
                                ], 'edd_price' ),
                                CreateContext::create( 'offer', [
                                    'price'           => '79.99',
                                    'itemOffered'     => 'Caldera Forms Custom Bundle: 3 Add-ons',
                                    'priceCurrency'   => 'USD',
                                    'priceValidUntil' => null,
                                    'url'             => 'https://calderaforms.com/caldera-forms-bundles/',
                                    'availability'    => 'http://schema.org/InStock'
                                ], 'edd_price' ),
                                CreateContext::create( 'offer', [
                                    'price'           => '139.99',
                                    'itemOffered'     => 'Caldera Forms Custom Bundle: 5 Add-ons',
                                    'priceCurrency'   => 'USD',
                                    'priceValidUntil' => null,
                                    'url'             => 'https://calderaforms.com/caldera-forms-bundles/',
                                    'availability'    => 'http://schema.org/InStock'
                                ], 'edd_price' ),
                                CreateContext::create( 'offer', [
                                    'price'           => edd_get_download_price( 20518 ),
                                    'itemOffered'     => 'Caldera Forms Agency Bundle',
                                    'priceCurrency'   => 'USD',
                                    'priceValidUntil' => null,
                                    'url'             => 'https://calderaforms.com/caldera-forms-bundles/',
                                    'availability'    => 'http://schema.org/InStock'
                                ], 'edd_price' ),
                                CreateContext::create( 'offer', [
                                    'price'           => edd_get_download_price( 48255 ),
                                    'itemOffered'     => 'Caldera Forms Enterprise Bundle',
                                    'priceCurrency'   => 'USD',
                                    'priceValidUntil' => null,
                                    'url'             => 'https://calderaforms.com/caldera-forms-bundles/',
                                    'availability'    => 'http://schema.org/InStock'
                                ], 'edd_price' ),
    
                            ];
    
                            $offerArg = [];
                            foreach ( $offers as $offer ){
                                $offerArg[] = $offer->getProperties();
                            }
                            $args[ 'offers' ] = $offerArg;
    
                            return $args;
                            
                        } );
    
                        return $properties;
    
                    }
                } );
    
            }
    
            return $creator;
    
        } );
    
    });


```

**Copyright 2017 CalderaWP LLC. License: GPL v2+**